package devdiary.gamedev.mapview;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity
    extends Activity
{
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);
    }
}