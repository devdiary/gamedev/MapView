![compileSdkVersion 26](https://img.shields.io/badge/compileSdkVersion-26-yellow.svg) ![buildToolsVersion 26.0.2](https://img.shields.io/badge/buildToolsVersion-26.0.2-blue.svg) ![minSdkVersion 15](https://img.shields.io/badge/minSdkVersion-15-red.svg) ![targetSdkVersion 25](https://img.shields.io/badge/targetSdkVersion-25-green.svg)

<h1 align="center">DeveloperDiary GameDev MapView</h1>

<div align="center">
  <img src="media/icon.png"/>
</div>
<div align="center">
  <strong>source for https://medium.com/developerdiary/gamedev</strong>
</div>

## :question: Tasks

- draw object with size (width, height)
- move camera
- zoom camera
